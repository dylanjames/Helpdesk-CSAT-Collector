# Helpdesk CSAT Suervey
I made this in quite literally less than 10 minutes for a small company. 

This express app is meant to be used with a helpdesk's automated response system for resolution.

# How I use it:
When a ticket is first resolved, wait 48 hours and then send a link asking if we did good or bad. 
- Good: http://your-domain.com/{ticket_number}/positive
- Bad: http://your-domain.com/{ticket_number}/negative

It then collects the feedback, and sends it via email back to the helpdesk using node mailer.

I made this for one purpose, and I won't go back and add little things that are unneeded on my end. However, you need to fill out the login credentials for you mail server in `./routes/ticket`. 

The from address must be the same as the account you are logging into.
